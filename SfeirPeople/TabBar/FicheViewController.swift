//
//  FicheViewController.swift
//  SfeirPeople
//
//  Created by Safia CHMITI on 16/08/2018.
//  Copyright © 2018 Safia CHMITI. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseDatabase

class FicheViewController: UIViewController {
    var refsferiens: DatabaseReference!
  
    @IBOutlet weak var modifierButton: UIButton!
    @IBOutlet weak var enregistrerButton: UIButton!
    
    @IBOutlet weak var nomTextField: UITextField!
    @IBOutlet weak var prenomTextField: UITextField!
    @IBOutlet weak var datenaissanceTextField: UITextField!
    @IBOutlet weak var genreTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var dateEntreeTextField: UITextField!
    @IBOutlet weak var fonctionTextField: UITextField!
    @IBOutlet weak var clientTextField: UITextField!
    @IBOutlet weak var entitéTextField: UITextField!
    @IBOutlet weak var mangerTextField: UITextField!
    @IBOutlet weak var competencesTextField: UITextField!
    @IBOutlet weak var githubTextField: UITextField!
    @IBOutlet weak var twitterTextField: UITextField!
    @IBOutlet weak var linkedInTextField: UITextField!
    
    @IBOutlet weak var DescriptionText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refsferiens = Database.database().reference().child("sfeirians")//.child("chmiti_m")
        refsferiens.observe(DataEventType.value) { (snapshot) in
            if snapshot.childrenCount>0 {
                if let childSnapshot = snapshot.childSnapshot(forPath: "\(self.returnid())") as? DataSnapshot{
                            print(self.returnid())
                            if let achievementDictionary = childSnapshot.value as? [String:AnyObject] , achievementDictionary.count > 0{
                                //childSnapshot.setValue("testtest", forKey: "nom")
                                self.nomTextField.text = achievementDictionary["nom"] as? String
                                self.prenomTextField.text = achievementDictionary["prénom"] as? String
                                self.datenaissanceTextField.text = achievementDictionary["date_de_naissance"] as? String
                                self.genreTextField.text = achievementDictionary["sexe"] as? String
                                self.dateEntreeTextField.text = achievementDictionary["date_d_entrée_chez_sfeir"] as? String
                                self.fonctionTextField.text = achievementDictionary["fonction_dans_l_entreprise"] as? String
                                self.clientTextField.text = achievementDictionary["client_actuel"] as? String
                                self.entitéTextField.text = achievementDictionary["entité_de_rattachement"] as? String
                                self.mangerTextField.text = achievementDictionary["manager"] as? String
                                self.competencesTextField.text = achievementDictionary["compétences_techniques_et_ou_métiers"] as? String
                                self.githubTextField.text = achievementDictionary["github"] as? String
                                self.linkedInTextField.text = achievementDictionary["url_linkedin"] as? String
                                self.twitterTextField.text = achievementDictionary["twitter"] as? String
                                self.emailTextField.text = achievementDictionary["adresse_email"] as? String
                                self.DescriptionText.text = achievementDictionary["présentation"] as? String
                        
                            }
                        }
               }
            }
        }
    

    func returnid() -> String{
        let iduser = Auth.auth().currentUser?.uid
        return iduser!
    }
  
    @IBAction func enregisterDonnee(_ sender: Any) {
        modifierButton.isHidden = false
        enregistrerButton.isHidden = true
        nomTextField.isEnabled = false
        refsferiens = Database.database().reference().child("sfeirians")
        refsferiens.child("\(returnid())").child("nom").setValue(self.nomTextField.text)
        refsferiens.child("\(returnid())").child("prénom").setValue(self.prenomTextField.text)
        refsferiens.child("\(returnid())").child("date_de_naissance").setValue(self.datenaissanceTextField.text)
        refsferiens.child("\(returnid())").child("sexe").setValue(self.genreTextField.text)
        refsferiens.child("\(returnid())").child("date_d_entrée_chez_sfeir").setValue(self.dateEntreeTextField.text)
        refsferiens.child("\(returnid())").child("fonction_dans_l_entreprise").setValue(self.fonctionTextField.text)
        refsferiens.child("\(returnid())").child("client_actue").setValue( self.clientTextField.text)
        refsferiens.child("\(returnid())").child("entité_de_rattachement").setValue( self.entitéTextField.text)
        refsferiens.child("\(returnid())").child("manager").setValue(self.mangerTextField.text)
        refsferiens.child("\(returnid())").child("compétences_techniques_et_ou_métiers").setValue(self.competencesTextField.text)
        refsferiens.child("\(returnid())").child("github").setValue(self.githubTextField.text)
        refsferiens.child("\(returnid())").child("url_linkedin").setValue(self.linkedInTextField.text)
        refsferiens.child("\(returnid())").child("twitter").setValue(self.twitterTextField.text)
        refsferiens.child("\(returnid())").child("adresse_email").setValue(self.emailTextField.text)
        refsferiens.child("\(returnid())").child("présentation").setValue(self.DescriptionText.text)
        }
    
    @IBAction func modifierFiche(_ sender: Any) {
        enregistrerButton.isHidden = false
        nomTextField.isEnabled = true
        prenomTextField.isEnabled = true
        datenaissanceTextField.isEnabled = true
        genreTextField.isEnabled = true
        emailTextField.isEnabled = true
        dateEntreeTextField.isEnabled = true
        fonctionTextField.isEnabled = true
        clientTextField.isEnabled = true
        entitéTextField.isEnabled = true
        mangerTextField.isEnabled = true
        competencesTextField.isEnabled = true
        githubTextField.isEnabled = true
        twitterTextField.isEnabled = true
        linkedInTextField.isEnabled = true
       
        
    }
    
   
    
    
}

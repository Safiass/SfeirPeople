//
//  Model.swift
//  SfeirPeople
//
//  Created by Safia CHMITI on 17/08/2018.
//  Copyright © 2018 Safia CHMITI. All rights reserved.
//

import Foundation
class Model{
    var name: String?
    var prenom: String?
    var email: String?
   var tel: String?
   var manager: String?
   var localisation: String?
   
    init(name: String, prenom: String, email: String, tel: String, manager: String, localisation: String){
        self.name = name
        self.prenom = prenom
        self.email = email
        self.tel =  tel
        self.manager = manager
        self.localisation = localisation
    }
}

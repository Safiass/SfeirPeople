//
//  CellTableViewCell.swift
//  SfeirPeople
//
//  Created by Safia CHMITI on 17/08/2018.
//  Copyright © 2018 Safia CHMITI. All rights reserved.
//

import UIKit

class CellTableViewCell: UITableViewCell {

    @IBOutlet weak var prenomLabelText: UILabel!
    @IBOutlet weak var nameLabelText: UILabel!
    @IBOutlet weak var localisationLabel: UILabel!
    @IBOutlet weak var managerText: UILabel!
    @IBOutlet weak var telephoneText: UILabel!
    @IBOutlet weak var emailText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

//
//  SferiensViewController.swift
//  SfeirPeople
//
//  Created by Safia CHMITI on 17/08/2018.
//  Copyright © 2018 Safia CHMITI. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class SferiensViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    @IBOutlet weak var sfeirientableview: UITableView!
    var sfeirtable = [Model]()
    var sfeirtablefilter = [Model]()
    var refsferiens: DatabaseReference!
    @IBOutlet weak var searchitem: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchbar()
        refsferiens = Database.database().reference().child("sfeirians")
        refsferiens.observe(DataEventType.value) { (snapshot) in
            if snapshot.childrenCount>0 {
                self.sfeirtable.removeAll()
                
                for sferiens in snapshot.children.allObjects as! [DataSnapshot] {
                    let sferienobject = sferiens.value as? [String: AnyObject]
                    let sferienname = sferienobject?["nom"]
                    let sferienprenom = sferienobject?["prénom"]
                    //let sferientelephone = sferienobject?["téléphone_fixe_pro"]
                    let sferienemail = sferienobject?["email_sfeir"]
                    let sferienmanager = sferienobject?["manager"]
                    let sferienlocalisation = sferienobject?["entité_de_rattachement"]
                    
                    let sferien = Model(name: sferienname as! String, prenom: sferienprenom as! String, email: sferienemail as! String, tel: "00123", manager: sferienmanager as! String, localisation: sferienlocalisation as! String )
                    
                    self.sfeirtable.append(sferien)
                    self.sfeirtablefilter = self.sfeirtable
                }
                self.sfeirientableview.reloadData()
               }
        }
        
    }
    func searchbar(){
      searchitem.delegate = self
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 330
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sfeirtablefilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CellTableViewCell
        let sferien: Model
        sferien = sfeirtablefilter[indexPath.row]
        
        cell.prenomLabelText.text = sferien.prenom
        cell.nameLabelText.text = sferien.name
        cell.emailText.text = sferien.email
       cell.telephoneText.text = sferien.tel
       cell.managerText.text = sferien.manager
       cell.localisationLabel.text = sferien.localisation
        return cell
        }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard !searchText.isEmpty else {
            sfeirtablefilter = sfeirtable
            sfeirientableview.reloadData()
            return
        }
        
        sfeirtablefilter = sfeirtable.filter({ (person) -> Bool in
            (person.name?.lowercased().contains(searchText.lowercased()))!
        })
        sfeirtablefilter = sfeirtable.filter({ (person) -> Bool in
            (person.prenom?.lowercased().contains(searchText.lowercased()))!
        })
        sfeirientableview.reloadData()
    }
    
    
    
    
}

//
//  LoginViewController.swift
//  SfeirPeople
//
//  Created by Safia CHMITI on 10/08/2018.
//  Copyright © 2018 Safia CHMITI. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class LoginViewController: UIViewController {

    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let childRef = Database.database().reference(withPath: "sfeirians")
        print("this is \(childRef)")
        loginButton.layer.cornerRadius = 13
        emailText.layer.borderWidth =  3
        emailText.layer.borderColor = colorWithRGBHex(0x40b3b0).cgColor
        passwordText.layer.borderWidth = 3
        passwordText.layer.borderColor = colorWithRGBHex(0x40b3b0).cgColor
        }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       
        }
 
    @IBAction func loginButton(_ sender: Any) {
        Auth.auth().signIn(withEmail:emailText.text!, password: passwordText.text!){
            (user,error) in
            if let error = error {
                if (self.emailText.text?.isEmpty)! || (self.passwordText.text?.isEmpty)! {
                    let alertEmptyField = UIAlertController(title: "Empty Fields", message: "You have to complete all the informations", preferredStyle: .alert)
                    let actionEmptyField = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    alertEmptyField.addAction(actionEmptyField)
                    self.present(alertEmptyField, animated: true, completion: nil)
                }else {
                    let alertEmptyField = UIAlertController(title: "error", message: "\(error)", preferredStyle: .alert)
                    let actionEmptyField = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    alertEmptyField.addAction(actionEmptyField)
                    self.present(alertEmptyField, animated: true, completion: nil)
                    
                }
            } else {
               //let alertsignIn = UIAlertController(title: "Welcome", message: "Welcome to SfeirPeopleApp", preferredStyle: .alert)
                //let actionsignIn = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                //alertsignIn.addAction(actionsignIn)
                //self.present(alertsignIn, animated: true, completion: nil)
                self.postonsegue()
                
               
            }
        }
    }
    
   
    @IBAction func forgetpwdButton(_ sender: Any) {
        Auth.auth().sendPasswordReset(withEmail: self.emailText.text!) { (error) in
            if let error = error {
                print("the error is :\(error)")
            } else {
                print("the email was sent")
            }
        }
    }
    
    
    func postonsegue(){
        performSegue(withIdentifier: "seguelogin", sender: Auth.auth().currentUser?.uid)
    }
    
    func colorWithRGBHex(_ hex: Int, alpha: Float = 1.0) -> UIColor {
        let r = Float((hex >> 16) & 0xFF)
        let g = Float((hex >> 8) & 0xFF)
        let b = Float((hex) & 0xFF)
        return UIColor(red: CGFloat(r / 255.0), green: CGFloat(g / 255.0), blue: CGFloat(b / 255.0), alpha: CGFloat(alpha))
    }
}
// 1) segue and uialertcontroller at the same time.
/* // s'authentifier
 
 1) email/compte non existant
 2) mot de passe erroné
 3) mot de passe oublié *Done with Email*
 4)Authentification. *DONE*
 5) Champs vides. *DONE*
 
 */

//
//  ViewController.swift
//  SfeirPeople
//
//  Created by Safia CHMITI on 10/08/2018.
//  Copyright © 2018 Safia CHMITI. All rights reserved.
//

import UIKit

class MainView: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var segmentedControl: SegmentedControlDesign!
    
    enum TabIndex: Int {
        case login = 0
        case register = 1
    }
    
    var currentViewController: UIViewController?
    lazy var firstChildTabVC: UIViewController? = {
        let firstChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "loginID")
        return firstChildTabVC
    }()
    lazy var secondChildTabVC : UIViewController? = {
        let secondChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "registerID")
        
        return secondChildTabVC
    }()
    
    
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControl.initUI()
        segmentedControl.selectedSegmentIndex = TabIndex.register.rawValue
        displayCurrentTab(TabIndex.register.rawValue)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let currentViewController = currentViewController {
            currentViewController.viewWillDisappear(animated)
        }
    }
    
    
    @IBAction func switchTabs(_ sender: UISegmentedControl) {
        self.currentViewController!.view.removeFromSuperview()
        self.currentViewController!.removeFromParentViewController()
        
        displayCurrentTab(sender.selectedSegmentIndex)
    }
    // MARK: - Switching Tabs Functions
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            
            self.addChildViewController(vc)
            vc.didMove(toParentViewController: self)
            
            vc.view.frame = self.mainView.bounds
            self.mainView.addSubview(vc.view)
            self.currentViewController = vc
            
        }
    }
    
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case TabIndex.login.rawValue :
            vc = firstChildTabVC
        case TabIndex.register.rawValue :
            vc = secondChildTabVC
        default:
            return nil
        }
        
        return vc
    }
    
}


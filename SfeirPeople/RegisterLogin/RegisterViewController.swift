//
//  RegisterViewController.swift
//  SfeirPeople
//
//  Created by Safia CHMITI on 10/08/2018.
//  Copyright © 2018 Safia CHMITI. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerButton.layer.cornerRadius = 7
        passwordText.layer.borderWidth = 3
        passwordText.layer.borderColor = colorWithRGBHex(0x40b3b0).cgColor
        emailText.layer.borderWidth = 3
        emailText.layer.borderColor = colorWithRGBHex(0x40b3b0).cgColor
        confirmPassword.layer.borderWidth = 3
        confirmPassword.layer.borderColor = colorWithRGBHex(0x40b3b0).cgColor
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
      
    }
    
    @IBAction func registerButton(_ sender: Any) {
        Auth.auth().createUser(withEmail: emailText.text!, password: passwordText.text!) { (user, error) in
            if let error = error {
                if (self.emailText.text?.isEmpty)! || (self.passwordText.text?.isEmpty)! || (self.confirmPassword.text?.isEmpty)! {
                   let alertEmptyField = UIAlertController(title: "Empty Fields", message: "You have to complete all the informations", preferredStyle: .alert)
                    let actionEmptyField = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    alertEmptyField.addAction(actionEmptyField)
                    self.present(alertEmptyField, animated: true, completion: nil)
                    print("number 1")
                    
                
                }else if !isValidEmail(email: self.emailText.text!) {
                    let alertEmail = UIAlertController(title: "Email Invalid", message: "Your email is Invalid", preferredStyle: .alert)
                    let actionEmail = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    alertEmail.addAction(actionEmail)
                    self.present(alertEmail, animated: true, completion: nil)
                    print("number 4")
                    
                }else if !isValidPassword(password: self.passwordText.text!){
                    let alertPassword = UIAlertController(title: "Incorrect Password", message: "Passeword must contain at least 8 characters at least 1 Alphabet and 1 Number", preferredStyle: .alert)
                    let actionPassword = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    alertPassword.addAction(actionPassword)
                    self.present(alertPassword, animated: true, completion: nil)
                    print("number 2")
                    
                }else if self.passwordText.text != self.confirmPassword.text {
                        let alertPassword = UIAlertController(title: "No compatible Password", message: "Your passwords are not the same", preferredStyle: .alert)
                        let actionPassword = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                        alertPassword.addAction(actionPassword)
                        self.present(alertPassword, animated: true, completion: nil)
                    print("number 3")
                    
                } else {
                    let welcomeAlert = UIAlertController(title: "Welcom", message: "Your account is created", preferredStyle: .alert)
                    let actionWelcome = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    welcomeAlert.addAction(actionWelcome)
                    self.present(welcomeAlert, animated: true, completion: nil)
                print("number 5")
                
                }
        } // fin error = error
        }
        
       /* // création d'un utilisateur: gestion des cas
        
        1) tout les champs doivent être remplie / si c'est pas le cas soit afficheer uialert ou bien le champ devient rouge. *DONE*
        2) les mots de passe doivent être les mêmes. *DONE*
        3) L'émail tapé doit être valide sinon afficher erreur DONE
        4) Compte existant
        5) Enregistrer email  et mdp dans la base de donnée . *DONE*
        
        */
        
        
func isValidEmail(email:String) -> Bool {
       let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
       let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with:email)
    }// fin func isValidEmail
    
    func isValidPassword(password: String) -> Bool {
        let emailRegEx = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with:password)
    } // fin func isValidPassword
}

    func colorWithRGBHex(_ hex: Int, alpha: Float = 1.0) -> UIColor {
        let r = Float((hex >> 16) & 0xFF)
        let g = Float((hex >> 8) & 0xFF)
        let b = Float((hex) & 0xFF)
        return UIColor(red: CGFloat(r / 255.0), green: CGFloat(g / 255.0), blue: CGFloat(b / 255.0), alpha: CGFloat(alpha))
    }

}
